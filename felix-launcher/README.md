A simple Felix launcher that takes bundles from the command line

can be used from gradle if you define the following configurations: 

configurations {
	scala
	bnd
	felix
	launcher
	runFelix { transitive = false }
}

dependencies {
	scala 'org.scala-lang:scala-library:2.10.3'
  bnd 'biz.aQute:bnd:1.50.0'
	felix 'org.apache.felix:org.apache.felix.main:4.2.1'
	launcher 'ap.test:osgi-launcher:0.0.1'
	runFelix <whatever you need for your test>
}

task run(type: JavaExec, dependsOn: 'bnd') {
	main 'ap.test.Launcher'
	classpath configurations.felix, configurations.scala, configurations.launcher
	args configurations.runFelix.files + 'build/osgi/<your-artifact>'
}

